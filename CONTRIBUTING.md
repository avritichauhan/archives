Participants of SV-COMP can file a pull request to this repository in order
to make their verifier available for the competition execution,
to the SV-COMP jury for inspection, and for later replication of the results.

For SV-COMP 2018, the archives are stored as
`2018/<verifier>.zip`, where `<verifier>` is
the identifier for the verifier,
i.e., there exists a file `<verifier>.xml`
in the repository with the benchmark definitions:
https://github.com/sosy-lab/sv-comp/tree/master/benchmark-defs

Validators use a prefix `val_`, and can be sym-linked to a verifier archive.

By filing a pull/merge request to this repository, contributors confirm
that the license for the archive is compatible with
the requirements of SV-COMP, as outlined in the rules
https://sv-comp.sosy-lab.org/2018/rules_2017-10-27.pdf
under section Competition Environment and Requirements / Verifier.